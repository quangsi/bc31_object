const DSSV = "DSSV";

// array chứ danh sách sv

var dssv = [];

var dataJson = localStorage.getItem(DSSV);
if (dataJson !== null) {
  var arrayDanhSachSinhVien = JSON.parse(dataJson);

  for (var index = 0; index < arrayDanhSachSinhVien.length; index++) {
    var item = arrayDanhSachSinhVien[index];
    var sv = new SinhVien(
      item.ma,
      item.ten,
      item.email,
      item.mk,
      item.toan,
      item.ly,
      item.hoa
    );
    dssv.push(sv);
  }
  renderDSSV(dssv);
}
//  hàm lưu danh sách sv vào localStorage
var luuLocalStorage = function (dssv) {
  var dssvJSON = JSON.stringify(dssv);
  localStorage.setItem(DSSV, dssvJSON);
};

// validation

function kiemTraMaSinhVien(dssv, maSv) {
  var viTri = timKiemViTri(dssv, maSv);
  if (viTri == -1) {
    document.getElementById("spanMaSV").innerText = "";
    return true;
  } else {
    document.getElementById("spanMaSV").innerText = "Mã sinh viên bị trùng";
    return false;
  }
}

function renderDSSV(arraySinhVien) {
  var contentHTML = "";

  for (var index = 0; index < arraySinhVien.length; index++) {
    var sv = arraySinhVien[index];
    var contentTR = ` <tr> 
                      <td>${sv.ma}</td>
                      <td>${sv.ten}</td>
                      <td>${sv.email}</td>
                      <td>${sv.tinhDTB()}</td>
                      <td> 
                      <button class="btn btn-danger" onclick="xoaSinhVien(${
                        sv.ma
                      })">Xoá</button>

                      <button class="btn btn-primary" onclick="layThongTinSinhVien(${
                        sv.ma
                      })" >Sửa</button>
                      </td>
                     </tr> `;
    contentHTML = contentHTML + contentTR;
  }

  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
}

function themSV() {
  //
  var sv = layThongTinTuForm();
  var isValid = kiemTraMaSinhVien(dssv, sv.ma);
  if (isValid) {
    // thêm
    dssv.push(sv);
    // render
    renderDSSV(dssv);
    // lưu xuống local    luuLocalStorage(dssv);
  }
}

function xoaSinhVien(maSV) {
  console.log("maSV: ", maSV);
  // từ mã sv =>  tìm  được index  ( duyệt mảng để tìm )

  var viTri = timKiemViTri(dssv, maSV);
  console.log("viTri: ", viTri);
  if (viTri !== -1) {
    // xoá đối tượng và render lại giao diện
    dssv.splice(viTri, 1);
    renderDSSV(dssv);
    luuLocalStorage(dssv);
  }

  // for (var index = 0; index < dssv.length; index++) {
  //   var item = dssv[index];
  //   if (item.ma == maSV) {
  //     viTri = index;
  //     break;
  //   }
  // }

  // sv cần tìm dssv[index]

  // xoá sv đó bằng splice(index,1)

  // gọi lại hàm render dssv để có giao diện mới nhất
}

function layThongTinSinhVien(maSV) {
  var viTri = timKiemViTri(dssv, maSV);
  console.log("viTri: ", viTri);

  if (viTri == -1) {
    // nếu không tìm thấy sinh viên hợp lệ thì dừng hàm layThongTinSinhVien()
    return;
  }

  var sinhVien = dssv[viTri];
  document.getElementById("txtMaSV").disabled = true;

  hienThiThongTinLenForm(sinhVien);
}

function capNhatSinhVien() {
  document.getElementById("txtMaSV").disabled = false;
  // bài tập về nhà

  // b1  : gọi lại hàm layThongTinTuForm

  // b2 : tìm ra index của sv dc sửa

  // b3 : dssv[index]  =   sv ( sv là giá trị trả về của hàm layThongTinTuForm )

  // b4: render lại giao diện

  // b5: cập nhật localStorage
}
