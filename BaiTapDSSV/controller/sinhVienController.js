function timKiemViTri(dssv, maSV) {
  //   var viTri;

  //   for (var index = 0; index < dssv.length; index++) {
  //     var item = dssv[index];
  //     if (item.ma == maSV) {
  //       return viTri;
  //     }
  //   }
  //   return -1;

  return dssv.findIndex(function (item) {
    return item.ma == maSV;
  });
}
var layThongTinTuForm = function () {
  var ma = document.getElementById("txtMaSV").value;
  var ten = document.getElementById("txtTenSV").value;
  var email = document.getElementById("txtEmail").value;
  var mk = document.getElementById("txtPass").value;
  var toan = document.getElementById("txtDiemToan").value * 1;
  var ly = document.getElementById("txtDiemLy").value * 1;
  var hoa = document.getElementById("txtDiemHoa").value * 1;

  //
  // truyền đúng thứ tự params đã định nghĩa ở lớp đối tượng
  var sv = new SinhVien(ma, ten, email, mk, toan, ly, hoa);
  return sv;
};

var hienThiThongTinLenForm = function (sv) {
  document.getElementById("txtMaSV").value = sv.ma;
  document.getElementById("txtTenSV").value = sv.ten;
  document.getElementById("txtEmail").value = sv.email;
  document.getElementById("txtPass").value = sv.mk;
  document.getElementById("txtDiemToan").value = sv.toan;
  document.getElementById("txtDiemLy").value = sv.ly;
  document.getElementById("txtDiemHoa").value = sv.hoa;
};
