function SinhVien(_name, _gender) {
  this.name = _name;
  this.gender = _gender;
  this.greeting = function () {
    console.log("hello");
  };
}

// var sv1 = {
//   name: "Alice",
//   gender: "Femail",
// };
var sv1 = new SinhVien("Alice", "Femail");
var sv2 = new SinhVien("Bob", "Male");
console.log("sv1", sv1.name);
console.log("sv2", sv2.name);
