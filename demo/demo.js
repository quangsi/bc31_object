// cat: name, gender, age
var name = "alice";
var cat1 = {
  name: "Miu",
  age: 2,
  gender: "Femail",
  child: {
    name: "Miu con",
  },
  speak: function () {
    console.log("meo meo", this.name);
  },
};
// dynamic key
name = "speak";
var value = cat1[name];
console.log("value", value);

cat1.speak();

// shallow copy
var cat2 = { ...cat1 };
cat2.name = "mun";
console.log("cat2", cat2);
console.log("cat1", cat1);
// pass by reference

var a = 2;
var b = a;
b = 10;

console.log(a);
