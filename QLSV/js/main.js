function inKetQua() {
  console.log("yes");

  var maSv = document.getElementById("txtMaSV").value;
  var tenSv = document.getElementById("txtTenSV").value;
  var loaiSv = document.getElementById("loaiSV").value;
  var diemToan = document.getElementById("txtDiemToan").value * 1;
  var diemVan = document.getElementById("txtDiemVan").value * 1;
  var sv = {
    ma: maSv,
    ten: tenSv,
    loai: loaiSv,
    diemToan: diemToan,
    diemVan: diemVan,
    tinhDTB: function () {
      return (this.diemToan + this.diemVan) / 2;
    },
    xepLoai: function () {
      var dtb = this.tinhDTB();
      if (dtb >= 8) {
        return "Giỏi";
      } else if (dtb >= 6.5) {
        return "Khá";
      } else if (dtb >= 5) {
        return "Trung bình";
      } else {
        return "Yếu";
      }
    },
  };
  var diemTb = sv.tinhDTB();
  console.log(diemTb);

  document.getElementById("spanMaSV").innerText = sv.ma;
  document.getElementById("spanTenSV").innerText = sv.ten;
  document.getElementById("spanLoaiSV").innerText = sv.loai;
  document.getElementById("spanDTB").innerText = sv.tinhDTB();
  document.getElementById("spanXepLoai").innerText = sv.xepLoai();
}
